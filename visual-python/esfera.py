#!/usr/bin/env python3
# coding: utf-8
# Author: Luis Americo Auyadermont Mu#oz
# Email: lauyader2011@gmail.com
# Twitter/Telegram: @lauyader
from vpython import *
import time
sphere()

for x in range(1,20):
	sphere(pos=vector(x,-1,0), color=color.red)
	time.sleep(1)
	#print (x) 
