#!/usr/bin/env python
# -*- coding: utf-8 -*-
#main.py

from gevent import monkey
monkey.patch_all()

import time, random
from threading import Thread
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, disconnect

#from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
thread = None


def background_stuff():
    """ Let's do it a bit cleaner """
    while True:
        # Determinando el lapso de tiempo
        time.sleep(5)
        # Asignadole el valor de tiempo a una variable
        tiempo = str(time.clock())
        # generar un numero aleatorio simulando un sensor de temperatura
        t=str(random.randint(1,100))
        #Por cada variable se le asigna un socket.emit
        socketio.emit('message', {'data': 'This is data', 'time': tiempo}, namespace='/test')
        socketio.emit('message2', {'data': 'This is data', 'num': t}, namespace='/test')



@app.route('/')
def index():
    titulo="Sensor de Temeperatura"
    global thread
    if thread is None:
        thread = Thread(target=background_stuff)
        thread.start()
    return render_template('temp.html', titulo=titulo)

@socketio.on('my event', namespace='/test')
def my_event(msg):
    print 'Tiempo:',msg['data']

@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')
	

@app.route('/grafica')
def grafica():
    return  render_template("grafica.html")	


@app.route('/capturejs')
def capturejs():
    return  render_template("capturejs.html")     
if __name__ == '__main__':
    socketio.run(app, debug=True, port=5000)


