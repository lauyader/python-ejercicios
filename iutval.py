# -*- coding: utf-8 -*-
# Librarys
from flask import Flask, render_template
 

# Variables
app = Flask(__name__)

# Settings



# Views
@app.route('/', methods=('GET', 'POST'))
def index():
    return render_template('name.html')


# Run
if __name__ == '__main__':
    app.run()
