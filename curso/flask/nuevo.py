# -*- coding: utf-8 -*-
# Librarys
from flask import Flask, render_template


# Variables
app = Flask(__name__)


# Views
@app.route('/')
def index():
	titulo="unefa"
	data=['dia', 'mes', 'year']
	autor="Luis Americo Auyadermont"
	return render_template('index.html', titulo=titulo, fecha=data, Autor=autor)
@app.route('/prueba')
def prueba():
	return 'Hola prueba'
# Run
if __name__ == '__main__':
    app.run(debug=True)

