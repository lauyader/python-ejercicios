# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

# Sentencia necesaria para Pygame
# Sino lo utiliza no funciona la aplicacion
pygame.init()

#Ajuste el tamaño de la ventana
ventana=pygame.display.set_mode((400,300))

#Le coloco un nombre a la ventana
pygame.display.set_caption("Hola mundo")

while True:
    for evento in pygame.event.get():
        if evento.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()