#!/usr/bin/env python
# -*- coding: utf-8 -*-
import operator





miDic={"a":2, "b":3, "d":1, "c":1}

mi_nuevo_dic = {}

resultado = sorted(miDic.items(), key=operator.itemgetter(0))

print resultado
## O resultado = sorted(miDic.items(), key=operator.itemgetter(1)) dependediendo de por que lo quieras ordenar

 

for item in resultado:

## Entre llaves se ingresa la nueva clave que se obtiene de cada elemento de la lista resultante

## y se pide el elemento 0 de cada uno que correspondería a la clave

    nueva_clave = item[0]

## Mientras que en la tupla la posicion 1 guarda la informacion correspondiente al valor

    nuevo_valor = item[1]

    mi_nuevo_dic [nueva_clave] = nuevo_valor

print mi_nuevo_dic



s = {'ben' : 2 , 'amy' : 3 , 'Zelda': 1 } 

#ordena por keya
sort_s = s.iterkeys()

print s
#ordena por values
#sort_v = s.itervalues()
#print sort_v