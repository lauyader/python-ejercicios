#!/usr/bin/env python

def Demorse(nombre):                #1
    if not(nombre.endswith(".mrs")):    #2
        nombre = nombre+".mrs"        #3
    origen=open(nombre,"r")            #4
    pospunto = nombre.find('.')        #5
    nombresalida = nombre[:pospunto]    #6
    nombresalida = nombresalida+'.txt'    #7
    destino = open(nombresalida,"a")    #8
    nm = {'.-': 'a','-...': 'b','-.-.': 'c','-..': 'd','.': 'e','..-.': 'f',
        '--.': 'g','....': 'h','..': 'i','.---': 'j','-.-': 'k','.-..': 'l',
        '--': 'm','-.': 'n','---': 'o','.--.': 'p','--.-': 'q',    '.-.': 'r',
        '...': 's','-': 't','..-': 'u','...-': 'v','.--': 'w','-..-': 'x',
        '-.--': 'y','--..': 'z','-----': '0','.----': '1','..---': '2',
        '...--': '3','....-': '4','.....': '5','-....': '6','--...': '7',
        '---..': '8','----.': '9','.-.-.-': '.','--..--': ',',
        '..--..': '?','.-..-.': '\"','----------': '-',}        #9

    for linea in origen.readlines():        #10
        lineasalida = mayusculas = letra = ''    #11
        mayu = haciendoletra = 0        #12
        for i in linea:                #13
            if i=='m' or i=='a' or i=='y' or i=='u': #14
                mayusculas = mayusculas+i    #15
                if mayusculas == 'mayu':    #16
                    mayu=1            #17
                    mayusculas=''        #18
            elif i=='.' or i=='-':            #19
                letra=letra+i            #20
                haciendoletra=1            #21
            elif i==' ' and haciendoletra and mayu==1:        #22
                lineasalida = lineasalida+nm[letra].upper()    #23
                haciendoletra = mayu = 0            #24
                letra=''                    #25
            elif i==' ' and haciendoletra and mayu==0:        #26
                lineasalida = lineasalida+nm[letra]        #27
                haciendoletra=0                    #28
                letra=''                    #29
            else:                            #30
                lineasalida = lineasalida + i            #31
                haciendoletra = mayu = 0            #32
                mayusculas = letra = ''                #33
        destino.write(lineasalida)    #34
    origen.close()                #35
    destino.close()                #36