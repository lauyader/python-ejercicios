#!/usr/bin/env python
# -*- coding: utf-8 -*-


cod={'a': ".-", "b": "-..." ,"c":"-.-.",   "d": "-..",     "e":".",    "f": "..-.","g":"--.","h":"....","i": "..","j":".---", "k": "-.-",    "l": ".-..",    "m":"--", "n":"-.", "o":"---", "p":".--.","q": "--.-", "r": ".-.",  "s":"...",  "t":"-", "u":"..-","v":"...-", "w":".--", "x":"-..-", "y": "-.--", "z":"--..", "": ""}  


#Capturar la expresion a convertir

texto= raw_input("Introduzca la expresion: ")

# Declaramos la lista para realizar la conversion

conv=[]

for x in texto:
	#falta convertir en minúsculas el valor de x
	#¿Cómo lo hariamos?
	if x in cod:
		valor=cod.get(x)
		print "Letra %s, conversion:   %s" %(x,valor)
		conv.append(valor)
	else:
		print "__"	

print "Conversion:",conv




