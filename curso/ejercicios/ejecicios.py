#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' 
    1.El nombre de usuario debe contener un mínimo de 6 caracteres y un máximo de 12/.
    2.El nombre de usuario debe ser alfanumérico.
    3.Nombre de usuario con menos de 6 caracteres, retorna el mensaje "El nombre de usuario debe contener al menos 6 caracteres".
    4.Nombre de usuario con más de 12 caracteres, retorna el mensaje "El nombre de usuario no puede contener más de 12 caracteres".
    5.Nombre de usuario con caracteres distintos a los alfanuméricos, retorna el mensaje "El nombre de usuario puede contener solo letras y números".
    6.Realizar un juego con funciones- '''


#Solución al Ejercicio 1

usuario = raw_input("Nombre del usuario:")


contar= len(usuario)
print "Cantidad de Caracteres es: ", contar
if contar>=6 and contar<=12:
    print "Aprobado"
else: 
    print "Reprobado"    
        
enter=raw_input("Ejericio No 2")

#Solución al Ejericio 2
 # Debemos de trabajar con la función var.isalnum(),alfanumérica   isalpha(), alfabetica 
 # isdigit(), numerica islower(), Minúscula  isupper(), Mayúscula isspace(), Tienes Espacio en blanco
 # istitle(), Si tiene formato de Titulo 

cadena=raw_input("Introduzca la cadena de caracteres: ")

 

if cadena.isalnum()==False:
    print "La cadena no es alfanumerica"
else:
    print "La cadena es alfanumerica"
    
# Ejercicio No 7
#        
juego= raw_input("De que color es una naranja:?")
 



def jugar(intento=1): 
    respuesta = raw_input("¿De qué color es una naranja? ") 
    if respuesta != "naranja": 
        if intento < 3: 
            print "\nFallaste! Inténtalo de nuevo" 
            intento += 1
            jugar(intento) # Llamada recursiva 
        else: 
            print "\nPerdiste!" 
    else:
        print "\nGanaste!" 
jugar()

