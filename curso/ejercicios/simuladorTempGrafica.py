#!/usr/bin/env python
# coding: utf-8
# Author: Luis Americo Auyadermont Mu#oz
# Email: lauyader2011@gmail.com
# Twitter/Telegram: @lauyader
# 
# Título: Capturar datos en un diccionario simulando un sensor de Temperatura


# Instalación de la librerias
## Asignar números aleatorios
import random

## Control del tiempo  time.sleep(No tiempo en segundo)
import time

# Graficar los datos
import matplotlib.pyplot as plt

#Declaracion de las variables
	#Iniciar el sensor en cero
sensor=0
	# Declarar el diccionario donde se almacenara los datos
datos={}





while True:
	#Inicializamos la varible hora, cada vez que iniciamos el dia
	hora = 0
	# Iniciar el dia que desea capturar datos
	dia= raw_input("Que dia es hoy o presione 'q' para salir:")
	if dia=='q':
		print "Haz salido del sistema"
		break
		
	mdia=dia.upper()
	datos[mdia]=[]
	
		

	while hora <= 24:
		#Generar el numero aleatorio
		sensor=random.randint(1,100)
		#Almacena en una lista los datos del sensor y 
		#luego lo almacena en el diccionario datos.
		
		datos[mdia].append(sensor)
		# Muestra los datos del sensor recibidoss
		print "sensor", sensor

		# Espera un lapso de un segundo para 
		# obtener del siguiente dato del sensor
		# hasta cumplir con el ciclo 24.
		# 
		#time.sleep(1)


		#Contador que asigna las 24 horas 
		hora+=1


	#print datos
	print "dia", datos[mdia]
plt.plot(datos[mdia])
plt.xlabel('Hora')
plt.ylabel('Temperatura')
plt.show()

print datos






